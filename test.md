# Shiny Server
Shiny hosts webapps in R programming language

https://www.rstudio.com/products/shiny/shiny-server/

Requestor: For Stephanie Labou

### NFS share:
- lib-freenas:/mnt/LibraryServices/k8stest

*=== KEEP ACCESS LIST UP TO DATE WITH LIST OF WORKERS ===*

Configured into ucsdshiny.yaml\
Pre-install examples from shiny server

### Samba Share:
- Corresponding Samba share `\\darry\shinytest`
- Access granted by AD group: `lib-shiny-rw`


### Build docker image and push to gitlab container repo:
```bash
docker login registry.gitlab.com -u <USERNAME> -p <TOKEN>
docker build -t registry.gitlab.com/ucsdlibrary/development/ucsd-shiny .
docker push registry.gitlab.com/ucsdlibrary/development/ucsd-shiny
```

### Deploy to kubernetes cluster based on kubeconfig and namespace:
```
kubectl --kubeconfig nonprod.yaml apply -f ucsdshiny.yaml -n nfstest
```

### Add ingress

- DNS name in ingress yaml
- `kubectl --kubeconfig nonprod.yaml apply -f ingress.yaml -n nfstest`
- http://lib-shiny-nonprod.ucsd.edu

Add the DNS name as CNAME (aka alias) to INFOBLOX for lib-worker-nonprod-lb (haproxy)
