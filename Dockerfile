FROM rocker/shiny-verse
RUN R -e "install.packages(c('plotly', 'shinythemes', 'DT', 'stringdist', 'rintrojs', 'shinycssloaders', 'writexl'),dependencies=TRUE)"
